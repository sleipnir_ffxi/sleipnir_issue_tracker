Sleipnir issue tracker

Please post all issues under here https://gitlab.com/sleipnir_ffxi/sleipnir_issue_tracker/issues 

Sleipnir is level 75 era server with a custom twist. 

Our goal is to give a bug-free gameplay experience to our players, with us fixing all missions and quests on an expansion-by-expansion basis, to work 100% like retail. That being said, we are currently a CoP based server, with the full intent to open ToAU, WoTG and the mini-expansions once RoZ and CoP are 100% finished. This includes jobs.

-------------------

Some of the changes we have/will have are:

Daily login rewards
This system currently works, just not implemented and no rewards added in yet.

Weekly hunts
This system currently works, just not implemented and no rewards added in yet.

A fully functional Mount system with our own custom quests & events for obtaining them
Note: As of this week DSP has added Mount functionality to core, but we were the only server prior to this with Mounts working as retail.

A retail-replicated “Sparks Vendor” system that uses Conquest Points instead of sparks.
Instead of relying on the Auction House, you will be able to use your Conquest Points to obtain level 1-70 gear from the vendors, ensuring you can always work towards the goal of gearing up and entering endgame. This is currently a WIP.

Custom Outpost Warps
Being a level 75 era server, there are no Homepoints to warp to - we would like you to explore the world! To replace them, we have customised outposts to not require the package delivery quest to obtain them. Instead, you simply go to the outpost, click on the NPC and unlock the ability to warp to it! Beastmen controlled areas are currently work in progress to add NPC.

We feel that with the additions of mounts and custom outposts, traveling around Vana’diel will be very simple, and bring back some of the adventure of exploring!
Alpha Stage Testing

As an alpha tester you will be rewarded not only with items on your main character after the server is launched, but you will be able to shape the future direction of this server! We are very early in development and are very open to new ideas as they come up.

The testing incentives will be based on how active you are in bug reporting, using a tiered system. Your activity will be decided by the developers but the incentives will be the same for all:

Tier 1 - TBC
Tier 2 - TBC
Tier 3 - TBC

We will be limiting Tier 3 to the highest 5 performing testers at the end of the testing period, and all decisions are made at the development team’s discretion. The team defines the criteria for a “fixable bug” based upon multiple factors, including, but not limited to; the possibility of a fix and whether it is a known issue to DSP Private Servers. All testing accounts will be deleted prior to launch).

-------------------

A few extra notes 

Any fixes and issues might not be able to be fixed immediately, so please be patient! We all have lives and jobs too.

